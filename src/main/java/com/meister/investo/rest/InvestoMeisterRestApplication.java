package com.meister.investo.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestoMeisterRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvestoMeisterRestApplication.class, args);
	}

}
