package com.meister.investo.rest.dao;

import com.meister.investo.rest.model.Alien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

/**
 * Created by vishalsharma on 16/05/20.
 */
@RepositoryRestResource(collectionResourceRel = "alien", path = "alien")
public interface AlienDao extends JpaRepository<Alien, Integer> {
}
