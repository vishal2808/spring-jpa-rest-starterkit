package com.meister.investo.rest.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by vishalsharma on 09/05/20.
 */
@Data
@Component
@Entity
public class Laptop {
    @Id
    private int id;
    private String brand;

    public Laptop() {
        System.out.println("========= Laptop Object created =========");
    }

    public void compile(){
        System.out.println("======= Compiling a new program =======");
    }
}
