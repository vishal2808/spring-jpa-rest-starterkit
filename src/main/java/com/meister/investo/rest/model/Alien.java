package com.meister.investo.rest.model;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;

/**
 * Created by vishalsharma on 09/05/20.
 */
@Data
@Component
@Entity
public class Alien {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    private String name;
    private String tech;

    @Autowired @Transient
    private Laptop laptop;

    public Alien() {
        System.out.println("========= Alien Object created =========");
    }

    public Alien(String name, String tech) {
        this.name = name;
        this.tech = tech;
    }

    public void show(){
        System.out.println("=========in show method of Alien Object=========");
        laptop.compile();
        laptop.setBrand("Apple aala");
    }
}
